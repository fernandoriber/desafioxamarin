# DesafioXamarin

Projeto de teste desenvolvido com as seguintes configurações:

Visual Studio 2019 XAMARIN FORMS 4.2.0.848062 NETStandard.Library 2.0.3 Prism 7.2

Proposta:

Foi disponibilizado uma proposta de teste para a criação de um aplictivo para exibição e manipulação de dados via API REST. Acredito que o teste proposto seria para um posição de Full-stack.

Porém, não havia como eu criar os serviços do back-end (tempo, hospedagem, etc), então desenvolvi o projeto simulando um cenário com as APIS já criadas, por esse motivo o projeto não está executando, nem consumindo nenhum serviço real, mas acredito que atende as necessidades de mostrar meus conhecimentos com desenvolvimento mobile Xamarin-Cross Platform.

Fernando Ribeiro.


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioXamarin.Behaviors
{
    public class UnmaskBehavior
    {
        public static string UnmaskField(string s)
        {
            return s.Replace(".", "").Replace("-", "")
                    .Replace("/", "").Replace("(", "")
                    .Replace(")", "");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioXamarin.Services
{
    public class HttpServices
    {
        public static string CONTENT_TYPE = "application/json";

        public static string URL_DEFAULT = "https://desafioproject.azurewebsites.net";

        //END-POINTS
        public static string Login = $"{URL_DEFAULT}/api/usuario/Login";

        public static string GetAllUsers = $"{URL_DEFAULT}/api/usuario/GetAll";

        public static string CreateUser = $"{URL_DEFAULT}/api/usuario/CreateUser";

        public static string UpdateUser = $"{URL_DEFAULT}/api/usuario/UpdateUser";

        public static string DeleteUser = $"{URL_DEFAULT}/api/usuario/DeleteUser";

        public static string GetEstados = $"{URL_DEFAULT}/api/estado/GetAll";

        public static string GetCidade = $"{URL_DEFAULT}/api/cidade/GetCidadePorEstadoId?";

        public static string GetCadastro = $"{URL_DEFAULT}/api/usuario/BuscarPorUsuarioId?";

    }
}

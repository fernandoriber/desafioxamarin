﻿using DesafioXamarin.Models.Cidade;
using DesafioXamarin.Models.Estado;
using DesafioXamarin.Models.User;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DesafioXamarin.Services.User
{
    public class UserServices
    {
        public async Task<UserModel> Login(string _email, string _password)
        {
            UserModel responseLogin = new UserModel();

            HttpClient oHttpClient = new HttpClient();
            oHttpClient.Timeout = new TimeSpan(0, 1, 0);

            var data = JsonConvert.SerializeObject(new { Login = _email, Senha = _password });

            string url = HttpServices.Login;

            var resp = await oHttpClient.PostAsync(url, new StringContent(data, Encoding.UTF8, HttpServices.CONTENT_TYPE));

            if (resp.IsSuccessStatusCode)
            {
                var requestLogin = resp.Content.ReadAsStringAsync().Result;
                var a = JObject.Parse(requestLogin);
                responseLogin = a.ToObject<UserModel>();
            }
            return responseLogin;
        }

        public async Task<List<UserModel>> GetAllUsersAsync()
        {
            List<UserModel> userModel = new List<UserModel>();

            HttpClient oHttpClient = new HttpClient();
            oHttpClient.Timeout = new TimeSpan(0, 1, 0);

            var ret = await oHttpClient.GetAsync(HttpServices.GetAllUsers);

            if (ret.IsSuccessStatusCode)
            {
                var request = ret.Content.ReadAsStringAsync().Result;

                userModel = JsonConvert.DeserializeObject<List<UserModel>>(request);
            }

            return userModel;
        }

        public async Task<List<EstadoModel>> GetEstadosAsync()
        {
            List<EstadoModel> estadoModel = new List<EstadoModel>();

            HttpClient oHttpClient = new HttpClient();
            oHttpClient.Timeout = new TimeSpan(0, 1, 0);

            var ret = await oHttpClient.GetAsync(HttpServices.GetEstados);

            if (ret.IsSuccessStatusCode)
            {
                var request = ret.Content.ReadAsStringAsync().Result;

                estadoModel = JsonConvert.DeserializeObject<List<EstadoModel>>(request);
            }

            return estadoModel;
        }

        public async Task<List<CidadeModel>> GetCidadesAsync(int estadoId)
        {
            List<CidadeModel> _cidadeModel = new List<CidadeModel>();

            HttpClient oHttpClient = new HttpClient();
            oHttpClient.Timeout = new TimeSpan(0, 1, 0);

            string url = string.Format("{0}EstadoId={1}", HttpServices.GetCidade, estadoId.ToString());

            var ret = await oHttpClient.GetAsync(url);

            if (ret.IsSuccessStatusCode)
            {
                var request = ret.Content.ReadAsStringAsync().Result;

                _cidadeModel = JsonConvert.DeserializeObject<List<CidadeModel>>(request);
            }

            return _cidadeModel;
        }

        public async Task<Tuple<bool, string>> CreateUserAsync(UserModel userModel)
        {  
            HttpClient oHttpClient = new HttpClient();
            oHttpClient.Timeout = new TimeSpan(0, 1, 0);

            var data = JsonConvert.SerializeObject(userModel);

            var resp = await oHttpClient.PostAsync(HttpServices.CreateUser, new StringContent(data, Encoding.UTF8, HttpServices.CONTENT_TYPE));

            if (resp.IsSuccessStatusCode)
            {
                return new Tuple<bool, string>(true, null);
            }
            else
            {
                var messageJson = await resp.Content.ReadAsStringAsync();

                var messageObj = JsonConvert.DeserializeObject<JObject>(messageJson);

                var message = (string)messageObj["Message"];

                return new Tuple<bool, string>(false, message);
            }
        }

        public async Task<bool> UpdateUserAsync(UserModel userModel)
        {
            HttpClient oHttpClient = new HttpClient();
            oHttpClient.Timeout = new TimeSpan(0, 1, 0);

            var data = JsonConvert.SerializeObject(userModel);

            var resp = await oHttpClient.PostAsync(HttpServices.UpdateUser, new StringContent(data, Encoding.UTF8, HttpServices.CONTENT_TYPE));

            return resp.IsSuccessStatusCode ? true : false;
        }

        public async Task<UserModel> RecuperarCadastroAsync(string userId)
        {
            UserModel usuarioModel = new UserModel();

            HttpClient oHttpClient = new HttpClient();
            oHttpClient.Timeout = new TimeSpan(0, 1, 0);

            string url = string.Format("{0}id={1}", HttpServices.GetCadastro, userId);

            var resp = await oHttpClient.GetAsync(url);

            if (resp.IsSuccessStatusCode)
            {
                var request = resp.Content.ReadAsStringAsync().Result;

                usuarioModel = JsonConvert.DeserializeObject<UserModel>(request);
            }

            return usuarioModel;
        }

        public async Task<bool> DeleteUserAsync(string email)
        {
            HttpClient oHttpClient = new HttpClient();
            oHttpClient.Timeout = new TimeSpan(0, 1, 0);

            var data = JsonConvert.SerializeObject(new
            {
                Email = email
            });

            string url = string.Format("{0}Login={1}", HttpServices.DeleteUser, email);

            var resp = await oHttpClient.PostAsync(url, new StringContent(data, Encoding.UTF8, HttpServices.CONTENT_TYPE));

            return resp.IsSuccessStatusCode;
        }
    }
}

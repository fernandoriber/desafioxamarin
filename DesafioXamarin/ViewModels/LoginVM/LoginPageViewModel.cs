﻿using Acr.UserDialogs;
using DesafioXamarin.Helpers;
using DesafioXamarin.Services.User;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Threading.Tasks;

namespace DesafioXamarin.ViewModels.LoginVM
{
    public class LoginPageViewModel : ViewModelBase
    {
        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        public DelegateCommand LoginCommand { get; set; }

        private readonly INavigationService _navigationService;

        public LoginPageViewModel(INavigationService navigationService )
             : base(navigationService)
        {
            this._navigationService = navigationService;

            this.LoginCommand = new DelegateCommand(async () => await Login());
        }

        private async Task Login()
        {
            try
            {   
                await _navigationService.NavigateAsync("MainPage");

                if (string.IsNullOrEmpty(Email))
                {
                    UserDialogs.Instance.Alert("Campo e-mail não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(Password))
                {
                    UserDialogs.Instance.Alert("Campo senha não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (!Utils.IsConnected())
                {
                    UserDialogs.Instance.Alert(Constants.ConexaoString, Constants.AppName, "OK");
                    return;
                }
                else
                {
                    UserDialogs.Instance.ShowLoading("Efetuando Login...");

                    UserServices userServices = new UserServices();
                    var responseLogin = await userServices.Login(Email, Password);

                    if (responseLogin != null)
                    {
                        UserDialogs.Instance.HideLoading();

                        await _navigationService.NavigateAsync("MainPage");
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        await UserDialogs.Instance.AlertAsync("Usuário ou senha inválidos. Tente novamente.", Constants.AppName, "OK");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                UserDialogs.Instance.Alert("Erro ao efetuar login: " + ex.Message, Constants.AppName, "OK");
            }
        }
    }
}

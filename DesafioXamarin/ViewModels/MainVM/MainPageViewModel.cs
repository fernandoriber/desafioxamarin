﻿using Acr.UserDialogs;
using DesafioXamarin.Helpers;
using DesafioXamarin.Models.User;
using DesafioXamarin.Services.User;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DesafioXamarin.ViewModels.MainVM
{
    public class MainPageViewModel : ViewModelBase
    {
        private UserModel _selectedUser;
        public UserModel SelectedUser
        {
            get { return _selectedUser; }
            set { SetProperty(ref _selectedUser, value); }
        }

        public ObservableCollection<UserModel> UsersCollection { get; set; }

        public DelegateCommand CreateCommand { get; set; }

        public DelegateCommand UpdateCommand { get; set; }

        public DelegateCommand DeleteCommand { get; set; }

        public DelegateCommand ExitCommand { get; set; }

        private readonly INavigationService _navigationService;

        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Cadastro de usuários";

            this._navigationService = navigationService;

            this.CreateCommand = new DelegateCommand(CreateUser);

            this.UpdateCommand = new DelegateCommand(UpdateUser);

            this.DeleteCommand = new DelegateCommand(DeleteUser);

            this.ExitCommand = new DelegateCommand(ExitApp);

            Task.Run(async () => { await GetUsersList(); });
        }

        private async Task GetUsersList()
        {
            try
            {
                if (!Utils.IsConnected())
                {
                    UserDialogs.Instance.HideLoading();
                    UserDialogs.Instance.Alert(Constants.ConexaoString, Constants.AppName, "OK");
                    return;
                }
                else
                {
                    UserDialogs.Instance.ShowLoading("Obtendo usuários cadastrados...");

                    UserServices userServices = new UserServices();

                    List<UserModel> usersList = new List<UserModel>();

                    usersList = await userServices.GetAllUsersAsync();

                    if (usersList.Count == 0)
                    {
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.Alert("Não há usuários cadastrados: ", Constants.AppName, "OK");
                    }
                    else
                    {
                        UsersCollection = new ObservableCollection<UserModel>(usersList);
                        RaisePropertyChanged(nameof(UsersCollection));
                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                UserDialogs.Instance.Alert("Erro ao obter a lista de usuários." + ex.Message, Constants.AppName, "OK");
                return;
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        private async void CreateUser()
        {
            await _navigationService.NavigateAsync("CadastroPage");
        }

        private async void UpdateUser()
        {
            try
            {               
                if (SelectedUser == null)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Selecione um usuário para alterar.", Constants.AppName, "OK");
                    return;
                }
                else
                {
                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("UserModel", SelectedUser);

                    await _navigationService.NavigateAsync("AlterarCadastroPage", navigationParams);
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                await UserDialogs.Instance.AlertAsync("Erro ao atualizar o cadastro", Constants.AppName, "OK");
                return;
            }
        }

        private async void DeleteUser()
        {
            try
            {
                if (SelectedUser == null)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Selecione um usuário para excluir.", Constants.AppName, "OK");
                    return;
                }
                else
                {
                    UserServices userServices = new UserServices();

                    if (!Utils.IsConnected())
                    {
                        UserDialogs.Instance.Alert(Constants.ConexaoString, Constants.AppName, "OK");
                        return;
                    }
                    else
                    {
                        bool DeleteUser = await userServices.DeleteUserAsync(SelectedUser.Email);

                        if (DeleteUser)
                        {
                            UserDialogs.Instance.HideLoading();
                            await UserDialogs.Instance.AlertAsync("Usuário excluído com sucesso!", Constants.AppName, "OK");

                            await GetUsersList();
                        }
                        else
                        {
                            UserDialogs.Instance.HideLoading();
                            await UserDialogs.Instance.AlertAsync("Erro ao excluir o cadastro", Constants.AppName, "OK");
                            return;
                        }
                    }
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                await UserDialogs.Instance.AlertAsync("Erro ao excluir o cadastro", Constants.AppName, "OK");
                return;
            }
        }
        
        //Atualiza lista após exclusão ou alteração.
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("GetList"))
            {
                if ((bool)parameters["GetList"])
                {
                    await GetUsersList();
                }
            }
        }

        private void ExitApp()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (await UserDialogs.Instance.ConfirmAsync("Tem certeza que deseja sair?", Constants.AppName, "Sim", "Não"))
                {
                    await _navigationService.GoBackAsync();
                }
            });
        }
    }
}

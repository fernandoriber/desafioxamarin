﻿using Acr.UserDialogs;
using DesafioXamarin.Behaviors;
using DesafioXamarin.Helpers;
using DesafioXamarin.Models.Cidade;
using DesafioXamarin.Models.Estado;
using DesafioXamarin.Models.User;
using DesafioXamarin.Services.User;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioXamarin.ViewModels.Cadastro
{
    public class CadastroPageViewModel : ViewModelBase
    {
        private string _txtNome;
        public string TxtNome
        {
            get { return _txtNome; }
            set { SetProperty(ref _txtNome, value); }
        }

        private string _txtCpf;
        public string TxtCpf
        {
            get { return _txtCpf; }
            set { SetProperty(ref _txtCpf, value); }
        }

        private string _txtCelular;
        public string TxtCelular
        {
            get { return _txtCelular; }
            set { SetProperty(ref _txtCelular, value); }
        }

        private string _txtEmail;
        public string TxtEmail
        {
            get { return _txtEmail; }
            set { SetProperty(ref _txtEmail, value); }
        }

        private string _txtLogin;
        public string TxtLogin
        {
            get { return _txtLogin; }
            set { SetProperty(ref _txtLogin, value); }
        }

        private string _txtSenha;
        public string TxtSenha
        {
            get { return _txtSenha; }
            set { SetProperty(ref _txtSenha, value); }
        }

        private string _txtSenhaConfirm;
        public string TxtSenhaConfirm
        {
            get { return _txtSenhaConfirm; }
            set { SetProperty(ref _txtSenhaConfirm, value); }
        }

        private string _txtDataNasc;
        public string TxtDataNasc
        {
            get { return _txtDataNasc; }
            set { SetProperty(ref _txtDataNasc, value); }
        }

        private EstadoModel _selectedEstado;
        public EstadoModel SelectedEstado
        {
            get { return _selectedEstado; }
            set { SetProperty(ref _selectedEstado, value); }
        }

        private CidadeModel _selectedCidade;
        public CidadeModel SelectedCidade
        {
            get { return _selectedCidade; }
            set { SetProperty(ref _selectedCidade, value); }
        }

        public ObservableCollection<EstadoModel> EstadosCollection { get; set; }

        public ObservableCollection<CidadeModel> CidadesCollection { get; set; }

        public DelegateCommand CreateUserCommand { get; set; }

        private readonly INavigationService _navigationService;

        public CadastroPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            this.CreateUserCommand = new DelegateCommand(async () => await ExecuteCreateUser());

            //Task.Run(async () => { await LoadEstados(); });
        }

        private async Task LoadEstados()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Carregando...");

                if (!Utils.IsConnected())
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync(Constants.ConexaoString, Constants.AppName, "OK");
                    return;
                }
                else
                {
                    UserServices userServices = new UserServices();
                    List<EstadoModel> estadoModel = new List<EstadoModel>();

                    estadoModel = await userServices.GetEstadosAsync();

                    if (estadoModel.Count != 0)
                    {
                        EstadosCollection = new ObservableCollection<EstadoModel>(estadoModel);
                        RaisePropertyChanged(nameof(EstadosCollection));
                        UserDialogs.Instance.HideLoading();
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        await UserDialogs.Instance.AlertAsync("Nenhum estado carregado.", Constants.AppName, "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                await UserDialogs.Instance.AlertAsync("Erro ao carregar os estados: " + ex.Message, Constants.AppName, "OK");
                return;
            }
        }

        public async void PickerEstado_SelectedIndexChanged(int selectedIndex)
        {
            if (SelectedEstado != null)
            {
                try
                {
                    await LoadCidades(SelectedEstado.EstadoId);
                }
                catch (Exception ex)
                {
                    await UserDialogs.Instance.AlertAsync("Erro ao carregar as cidades: " + ex.Message, Constants.AppName, "OK");
                    return;
                }
            }
        }

        private async Task LoadCidades(int estadoId)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Carregando cidades...");

                if (!Utils.IsConnected())
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync(Constants.ConexaoString, Constants.AppName, "OK");
                    return;
                }
                else
                {
                    UserServices _usuarioServices = new UserServices();
                    List<CidadeModel> cidades = new List<CidadeModel>();

                    cidades = await _usuarioServices.GetCidadesAsync(estadoId);

                    if (cidades.Count != 0)
                    {
                        CidadesCollection = new ObservableCollection<CidadeModel>(cidades);
                        RaisePropertyChanged(nameof(CidadesCollection));
                        UserDialogs.Instance.HideLoading();
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        await UserDialogs.Instance.AlertAsync("Nenhuma cidade carregada.", Constants.AppName, "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                await UserDialogs.Instance.AlertAsync("Erro ao carregar as cidades: " + ex.Message, Constants.AppName, "OK");
                return;
            }
        }

        private async Task ExecuteCreateUser()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Processando...");

                if (string.IsNullOrEmpty(TxtNome))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo nome não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtCpf))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo CPF não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (!Utils.IsCPF(UnmaskBehavior.UnmaskField(TxtCpf)))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Informe um número de CPF válido.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtCelular))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo celular não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (UnmaskBehavior.UnmaskField(TxtCelular).Length < 11)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Informe um número de celular válido.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtEmail))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo email não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (!Utils.ValidateEmail(TxtEmail))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Informe um email válido.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtLogin))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo login não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtSenha))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo senha não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (!Utils.ValidatePassword(TxtSenha))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("A Senha deve conter o Formato:\n " +
                            " - Mínimo de 6 caracteres;\n" +
                            " - 1 letra maiúscula;\n" +
                            " - 1 número", "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtSenhaConfirm))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo de confirmação de senha não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (TxtSenhaConfirm != TxtSenha)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("As senhas não conferem.", Constants.AppName, "OK");
                    return;
                }
                else if (SelectedEstado == null)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Selecione um estado.", Constants.AppName, "OK");
                    return;
                }
                else if (SelectedCidade == null)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Selecione uma cidade.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtDataNasc))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo de data de nascimento não informado.", Constants.AppName, "OK");
                    return;
                }
                else
                {
                    UserServices userServices = new UserServices();

                    var usuarioModel = new UserModel()
                    {
                        Nome = TxtNome,
                        Cpf = TxtCpf,
                        Telefone = TxtCelular,
                        Email = TxtEmail,
                        Cidade = SelectedCidade,
                        Estado = SelectedEstado,
                        Login = TxtLogin,
                        Senha = TxtSenha,
                        ConfirmSenha = TxtSenhaConfirm,
                        DtNascimento = TxtDataNasc,
                        //Foto = 
                    };

                    var CreateUserStatus = await userServices.CreateUserAsync(usuarioModel);

                    if (CreateUserStatus.Item1)
                    {
                        UserDialogs.Instance.HideLoading();
                        await UserDialogs.Instance.AlertAsync("Cadastro efetuado com sucesso!", Constants.AppName, "OK");
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        await UserDialogs.Instance.AlertAsync(CreateUserStatus.Item2, Constants.AppName, $"OK");
                    }
                }

            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                await UserDialogs.Instance.AlertAsync("Erro ao realizar o cadastro", Constants.AppName, "OK");
            }
        }
    }
}

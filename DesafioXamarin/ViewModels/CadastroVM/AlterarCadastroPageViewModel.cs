﻿using Acr.UserDialogs;
using DesafioXamarin.Behaviors;
using DesafioXamarin.Helpers;
using DesafioXamarin.Models.Cidade;
using DesafioXamarin.Models.Estado;
using DesafioXamarin.Models.User;
using DesafioXamarin.Services.User;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioXamarin.ViewModels.CadastroVM
{
    public class AlterarCadastroPageViewModel : ViewModelBase
    {
        private string _txtNome;
        public string TxtNome
        {
            get { return _txtNome; }
            set { SetProperty(ref _txtNome, value); }
        }

        private string _txtCpf;
        public string TxtCpf
        {
            get { return _txtCpf; }
            set { SetProperty(ref _txtCpf, value); }
        }

        private string _txtCelular;
        public string TxtCelular
        {
            get { return _txtCelular; }
            set { SetProperty(ref _txtCelular, value); }
        }

        private string _txtEmail;
        public string TxtEmail
        {
            get { return _txtEmail; }
            set { SetProperty(ref _txtEmail, value); }
        }

        private string _txtLogin;
        public string TxtLogin
        {
            get { return _txtLogin; }
            set { SetProperty(ref _txtLogin, value); }
        }

        private string _txtSenha;
        public string TxtSenha
        {
            get { return _txtSenha; }
            set { SetProperty(ref _txtSenha, value); }
        }

        private string _txtSenhaConfirm;
        public string TxtSenhaConfirm
        {
            get { return _txtSenhaConfirm; }
            set { SetProperty(ref _txtSenhaConfirm, value); }
        }

        private string _txtDataNasc;
        public string TxtDataNasc
        {
            get { return _txtDataNasc; }
            set { SetProperty(ref _txtDataNasc, value); }
        }

        private EstadoModel _selectedEstado;
        public EstadoModel SelectedEstado
        {
            get { return _selectedEstado; }
            set { SetProperty(ref _selectedEstado, value); }
        }

        private CidadeModel _selectedCidade;
        public CidadeModel SelectedCidade
        {
            get { return _selectedCidade; }
            set { SetProperty(ref _selectedCidade, value); }
        }

        private UserModel _userModel;
        public UserModel UserModel
        {
            get { return _userModel; }
            set { SetProperty(ref _userModel, value); }
        }

        public List<EstadoModel> ListEstados { get; set; }
        public List<CidadeModel> ListCidades { get; set; }

        public bool IsLoad { get; set; }

        public ObservableCollection<EstadoModel> EstadosCollection { get; set; }

        public ObservableCollection<CidadeModel> CidadesCollection { get; set; }

        public DelegateCommand UpdateCommand { get; set; }

        public DelegateCommand CancelCommand { get; set; }

        private readonly INavigationService _navigationService;

        public AlterarCadastroPageViewModel(INavigationService navigationService, NavigationParameters parameters)
            : base(navigationService)
        {
            Title = "Alteração de cadastro";

            this._navigationService = navigationService;

            this.UpdateCommand = new DelegateCommand(async () => await ExecuteUpdate());

            this.CancelCommand = new DelegateCommand(async () => await ExecuteCancel());

            Task.Run(async () => { await LoadUserData(parameters); });

        }

        public async void PickerEstado_SelectedIndexChanged(int selectedIndex)
        {
            //Caso for load, apenas preenche o Picker com a cidade recuperada
            if (SelectedEstado != null && !IsLoad)
            {
                try
                {
                    await LoadCidades(SelectedEstado.EstadoId);
                }
                catch (Exception ex)
                {
                    await UserDialogs.Instance.AlertAsync("Erro ao carregar as cidades: " + ex.Message, Constants.AppName, "OK");
                    return;
                }
            }
        }

        public void PickerCidade_SelectedIndexChanged(int selectedIndex) { }

        private async Task LoadUserData(NavigationParameters parameters)
        {
            try
            {
                UserModel userModelReceived = new UserModel();

                if (parameters.ContainsKey("UserModel"))
                {
                    userModelReceived = (UserModel)parameters["UserModel"];
                }
                UserDialogs.Instance.ShowLoading("Carregando informações...");

                if (!Utils.IsConnected())
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync(Constants.ConexaoString, Constants.AppName, "OK");
                    return;
                }
                else
                {
                    IsLoad = true;

                    UserServices _usuarioServices = new UserServices();

                    var usuarioModel = await _usuarioServices.RecuperarCadastroAsync(userModelReceived.Login);

                    if (usuarioModel != null)
                    {

                        TxtNome = usuarioModel.Nome;
                        TxtCpf = usuarioModel.Cpf;
                        TxtCelular = usuarioModel.Telefone;
                        TxtEmail = usuarioModel.Email;
                        TxtEmail = usuarioModel.Login;
                        TxtSenha = usuarioModel.Senha;
                        TxtSenhaConfirm = usuarioModel.ConfirmSenha;
                        TxtDataNasc = usuarioModel.DtNascimento;


                        //Load Estados
                        ListEstados = await _usuarioServices.GetEstadosAsync();

                        //Set o estado do usuário no picker
                        if (ListEstados.Count != 0)
                        {
                            EstadosCollection = new ObservableCollection<EstadoModel>(ListEstados);
                            RaisePropertyChanged(nameof(EstadosCollection));

                            SelectedEstado = ListEstados.Where(x => x.EstadoId == UserModel.Cidade.EstadoId).First();
                        }
                        else
                        {
                            UserDialogs.Instance.HideLoading();
                            await UserDialogs.Instance.AlertAsync("Erro ao obter lista de estados.", Constants.AppName, "OK");
                        }

                        //Load Cidades
                        ListCidades = await _usuarioServices.GetCidadesAsync(UserModel.Cidade.CidadeId);

                        //Set a cidade do usuário no picker
                        if (ListCidades.Count != 0)
                        {
                            CidadesCollection = new ObservableCollection<CidadeModel>(ListCidades);
                            RaisePropertyChanged(nameof(CidadesCollection));

                            SelectedCidade = ListCidades.Where(x => x.CidadeId == UserModel.Cidade.CidadeId).First();
                        }
                        else
                        {
                            UserDialogs.Instance.HideLoading();
                            await UserDialogs.Instance.AlertAsync("Erro ao obter a lista de cidades.", Constants.AppName, "OK");
                        }
                    }
                    else
                    {
                        await UserDialogs.Instance.AlertAsync("Erro ao consultar os dados de usuário.", Constants.AppName, "OK");
                    }
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                await UserDialogs.Instance.AlertAsync("Erro ao obter os dados de usuário.", Constants.AppName, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }

            IsLoad = false;
        }

        private async Task LoadCidades(int estadoId)
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Carregando cidades...");

                if (!Utils.IsConnected())
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync(Constants.ConexaoString, Constants.AppName, "OK");
                    return;
                }
                else
                {
                    UserServices _usuarioServices = new UserServices();
                    List<CidadeModel> cidades = new List<CidadeModel>();

                    cidades = await _usuarioServices.GetCidadesAsync(estadoId);

                    if (cidades.Count != 0)
                    {
                        CidadesCollection = new ObservableCollection<CidadeModel>(cidades);
                        RaisePropertyChanged(nameof(CidadesCollection));
                        UserDialogs.Instance.HideLoading();
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        await UserDialogs.Instance.AlertAsync("Nenhuma cidade carregada.", Constants.AppName, "OK");
                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                await UserDialogs.Instance.AlertAsync("Erro ao carregar as cidades: " + ex.Message, Constants.AppName, "OK");
                return;
            }
        }

        private async Task ExecuteUpdate()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Processando...");

                if (string.IsNullOrEmpty(TxtNome))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo nome não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtCpf))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo CPF não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (!Utils.IsCPF(UnmaskBehavior.UnmaskField(TxtCpf)))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Informe um número de CPF válido.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtCelular))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo celular não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (UnmaskBehavior.UnmaskField(TxtCelular).Length < 11)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Informe um número de celular válido.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtEmail))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo email não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (!Utils.ValidateEmail(TxtEmail))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Informe um email válido.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtLogin))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo login não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtSenha))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo senha não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (!Utils.ValidatePassword(TxtSenha))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("A Senha deve conter o Formato:\n " +
                            " - Mínimo de 6 caracteres;\n" +
                            " - 1 letra maiúscula;\n" +
                            " - 1 número", "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtSenhaConfirm))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo de confirmação de senha não informado.", Constants.AppName, "OK");
                    return;
                }
                else if (TxtSenhaConfirm != TxtSenha)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("As senhas não conferem.", Constants.AppName, "OK");
                    return;
                }
                else if (SelectedEstado == null)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Selecione um estado.", Constants.AppName, "OK");
                    return;
                }
                else if (SelectedCidade == null)
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Selecione uma cidade.", Constants.AppName, "OK");
                    return;
                }
                else if (string.IsNullOrEmpty(TxtDataNasc))
                {
                    UserDialogs.Instance.HideLoading();
                    await UserDialogs.Instance.AlertAsync("Campo de data de nascimento não informado.", Constants.AppName, "OK");
                    return;
                }
                else
                {
                    UserServices userServices = new UserServices();

                    UserModel.Cidade = SelectedCidade;
                    UserModel.Estado = SelectedEstado;
                    UserModel.Nome = TxtNome;
                    UserModel.Cpf = TxtCpf;
                    UserModel.Telefone = TxtCelular;
                    UserModel.Email = TxtEmail;
                    UserModel.Cidade = SelectedCidade;
                    UserModel.Estado = SelectedEstado;
                    UserModel.Login = TxtLogin;
                    UserModel.Senha = TxtSenha;
                    UserModel.ConfirmSenha = TxtSenhaConfirm;
                    UserModel.DtNascimento = TxtDataNasc;

                    if (await userServices.UpdateUserAsync(UserModel))
                    {
                        UserDialogs.Instance.HideLoading();
                        await UserDialogs.Instance.AlertAsync("Cadastro alterado com sucesso!", Constants.AppName, "OK");

                        var parameters = new NavigationParameters();
                        parameters.Add("GetList", true);

                        await _navigationService.GoBackAsync(parameters);
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        await UserDialogs.Instance.AlertAsync("Erro ao atualizar o cadastro", Constants.AppName, "OK");
                        return;
                    }
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.HideLoading();
                await UserDialogs.Instance.AlertAsync("Erro ao atualizar o cadastro", Constants.AppName, "OK");
                return;
            }
        }
        private async Task ExecuteCancel()
        {
            var parameters = new NavigationParameters();
            parameters.Add("GetList", true);

            await _navigationService.GoBackAsync(parameters);
        }
    }
}

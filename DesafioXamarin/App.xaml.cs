﻿using Prism;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.DryIoc;
using DesafioXamarin.Views.Main;
using DesafioXamarin.Views.Login;
using DesafioXamarin.ViewModels.LoginVM;
using DesafioXamarin.ViewModels.MainVM;
using DesafioXamarin.Views.Cadastro;
using DesafioXamarin.ViewModels.Cadastro;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DesafioXamarin
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() 
            : this(null)
        {

        }

        public App(IPlatformInitializer initializer)
             : this(initializer, true)
        {

        }

        public App(IPlatformInitializer initializer, bool setFormsDependencyResolver)
           : base(initializer, setFormsDependencyResolver)
        {

        }

        protected override async void OnInitialized()
        {
            InitializeComponent();

#if DEBUG
            HotReloader.Current.Run(this);
#endif

            await NavigationService.NavigateAsync("NavigationPage/LoginPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<CadastroPage, CadastroPageViewModel>();
            containerRegistry.RegisterForNavigation<AlterarCadastroPage, AlterarCadastroPageViewModel>();
        }
    }
}

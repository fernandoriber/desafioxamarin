﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioXamarin.Models.Estado
{
    public class EstadoModel
    {
        public int EstadoId { get; set; }

        public string Sigla { get; set; }

        public string Descricao { get; set; }
    }
}

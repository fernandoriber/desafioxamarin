﻿using DesafioXamarin.Models.Cidade;
using DesafioXamarin.Models.Estado;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioXamarin.Models.User
{
    public class UserModel
    {
        public string Nome { get; set; }

        public string Cpf { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public CidadeModel Cidade { get; set; }

        public EstadoModel Estado { get; set; }

        public string Login { get; set; }

        public string Senha { get; set; }

        public string ConfirmSenha { get; set; }

        public string DtNascimento { get; set; }

        public string Foto { get; set; }       
    }
}

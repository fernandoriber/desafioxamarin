﻿using DesafioXamarin.Models.Estado;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioXamarin.Models.Cidade
{
    public class CidadeModel
    {
        public int CidadeId { get; set; }

        public int EstadoId { get; set; }

        public string Descricao { get; set; }

        public EstadoModel Estado { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Essentials;

namespace DesafioXamarin.Helpers
{
    public class Utils
    {

        public static bool IsConnected()
        {
            var current = Connectivity.NetworkAccess;

            return (current == NetworkAccess.Internet ? true : false);
        }

        public static bool IsCPF(string CPF)
        {
            if (string.IsNullOrEmpty(CPF) || CPF.Equals("00000000000") || CPF.Equals("11111111111") ||
                CPF.Equals("22222222222") || CPF.Equals("33333333333") || CPF.Equals("44444444444") ||
                CPF.Equals("55555555555") || CPF.Equals("66666666666") || CPF.Equals("77777777777") ||
                CPF.Equals("88888888888") || CPF.Equals("99999999999") || CPF.Length != 11)
            {
                return false;
            }

            char dig10, dig11;
            int sm;
            int r;
            int num;
            int peso;

            try
            {
                sm = 0;
                peso = 10;

                var X = CPF.ToCharArray();

                for (int i = 0; i < 9; i++)
                {
                    num = (int)X[i] - 48;
                    sm = sm + (num * peso);
                    peso--;
                }

                r = 11 - (sm % 11);
                if (r == 10 || r == 11)
                {
                    dig10 = '0';
                }
                else
                {
                    dig10 = (char)(r + 48);
                }

                sm = 0;
                peso = 11;
                for (int i = 0; i < 10; i++)
                {
                    num = (int)X[i] - 48;
                    sm = sm + (num * peso);
                    peso--;
                }

                r = 11 - (sm % 11);
                if (r == 10 || r == 11)
                {
                    dig11 = '0';
                }
                else
                {
                    dig11 = (char)(r + 48);
                }

                if ((dig10 == X[9]) && (dig11 == X[10]))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }

        }

        public static bool ValidatePassword(string Password)
        {
            var regex = new Regex("(?=.*[A-Z]).{6,}");
            var isValidPass = regex.Match(Password);

            return (isValidPass.Success ? true : false);
        }

        public static bool ValidateEmail(string Email)
        {
            return Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }
    }
}

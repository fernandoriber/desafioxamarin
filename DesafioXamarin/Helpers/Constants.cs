﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioXamarin.Helpers
{
    public static class Constants
    {
        public static string AppName = "Desafio Xamarin";

        public static string ConexaoString = "Verifique sua conexão com a internet e tente novamente.";
    }
}

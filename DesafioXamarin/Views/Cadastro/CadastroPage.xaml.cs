﻿using DesafioXamarin.ViewModels.Cadastro;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace DesafioXamarin.Views.Cadastro
{
    public partial class CadastroPage : PopupPage
    {
        public CadastroPage()
        {
            InitializeComponent();
        }

        private void PickerEstado_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ((CadastroPageViewModel)BindingContext)?.PickerEstado_SelectedIndexChanged(PickerEstado.SelectedIndex);
        }
    }
}

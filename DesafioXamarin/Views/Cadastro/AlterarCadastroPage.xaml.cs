﻿using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace DesafioXamarin.Views.Cadastro
{
    public partial class AlterarCadastroPage : PopupPage
    {
        public AlterarCadastroPage()
        {
            InitializeComponent();
        }
        private void PickerEstado_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ((AlteracaoCadastroPageViewModel)BindingContext)?.PickerEstado_SelectedIndexChanged(PickerEstado.SelectedIndex);
        }

        private void PickerCidade_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ((AlteracaoCadastroPageViewModel)BindingContext)?.PickerCidade_SelectedIndexChanged(PickerCidade.SelectedIndex);
        }
    }
}
